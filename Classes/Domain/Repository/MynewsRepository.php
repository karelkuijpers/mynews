<?php

namespace Parousia\Mynews\Domain\Repository;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
//\GeorgRinger\News\Domain\Repository\NewsRepository

class MynewsRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
	private $querySettings;

    /*
     * Constructs a new querySettings
     * @param \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings $querySettings
    */
    public function __construct(\TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings $querySettings)
    {
        $this->querySettings = $querySettings;
    } 

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
    */
    public function findEvents()
    {
 		$this->objectType = "GeorgRinger\News\Domain\Model\News";
        $this->querySettings->setRespectStoragePage(FALSE);
        // don't add sys_language_uid constraint
        $this->querySettings->setRespectSysLanguage(FALSE);
        $this->setDefaultQuerySettings($this->querySettings);

        $query = $this->createQuery();

		$eventendlimit=new \DateTime("- 1 hour");
		$eventstartlimit= new \DateTime("+ 70 days");
	    $query->matching(
			$query->logicalAnd(
				$query->logicalOr(
		                $query->greaterThanOrEqual('endtime', $GLOBALS['SIM_EXEC_TIME']),
		                $query->equals('endtime', 0)
						),
				$query->logicalOr(
		                $query->lessThanOrEqual('starttime', $GLOBALS['SIM_EXEC_TIME']),
		                $query->equals('starttime', 0)
						),
				$query->equals('isEvent', 1),
				$query->logicalOr(
					$query->greaterThanOrEqual('eventStart',$eventendlimit->getTimestamp()),
	                $query->greaterThanOrEqual('eventEnd', $GLOBALS['SIM_EXEC_TIME']),
					),
				$query->lessThanOrEqual('eventStart',$eventstartlimit->getTimestamp())
			)
		); 
		$query->setOrderings(['eventStart'=>\TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING]);
/*		$queryParser = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\Storage\Typo3DbQueryParser::class);
		 \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($queryParser->convertQueryToDoctrineQueryBuilder($query)->getSQL());
		 \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($queryParser->convertQueryToDoctrineQueryBuilder($query)->getParameters()); */
		try {
		      $result= $query->execute();
		} catch(Exception $e) {
		 	echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	
		try {
		      return $result;
		} catch(Exception $e) {
		 	echo 'Caught exception: ',  $e->getMessage(), "\n";
		} 
	}
 
}

