<?php

namespace Parousia\Mynews\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Georg Ringer, montagmorgen.at
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * News
 */
class News extends \GeorgRinger\News\Domain\Model\News
{

    /**
     * isEvent
     *
     * @var bool
     */
    protected $isEvent = false;

    /**
     * eventStart
     *
     * @var \DateTime
     */
    protected $eventStart = null;


    /**
     * eventEnd
     *
     * @var \DateTime
     */
    protected $eventEnd = null;

    /**
     * Returns the isEvent
     *
     * @return bool $isEvent
     */
    public function getIsEvent()
    {
        return $this->isEvent;
    }

    /**
     * Sets the isEvent
     *
     * @param bool $isEvent
     * @return void
     */
    public function setIsEvent($isEvent)
    {
        $this->isEvent = $isEvent;
    }

    /**
     * Returns the boolean state of isEvent
     *
     * @return bool
     */
    public function isIsEvent()
    {
        return $this->isEvent;
    }

    /**
     * Returns the eventStart
     *
     * @return \DateTime $eventStart
     */
    public function getEventStart()
    {
        return $this->eventStart;
    }

    /**
     * Sets the eventStart
     *
     * @param \DateTime $eventStart
     * @return void
     */
    public function setEventStart(\DateTime $eventStart)
    {
        $this->eventStart = $eventStart;
    }

    /**
     * Returns the eventEnd
     *
     * @return \DateTime $eventEnd
     */
    public function getEventEnd()
    {
        return $this->eventEnd;
    }

    /**
     * Sets the eventEnd
     *
     * @param \DateTime $eventEnd
     * @return void
     */
    public function setEventEnd(\DateTime $eventEnd)
    {
        $this->eventEnd = $eventEnd;
    }


}
