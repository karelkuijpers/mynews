<?php

namespace Parousia\Mynews\Controller;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use GeorgRinger\News\Domain\Model\Dto\NewsDemand;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Parousia\mynews\Controller\NewsController
 */
class MynewsController extends ActionController
{
    /**
     * @var Parousia\Mynews\Domain\Repository\MynewsRepository
     */
	protected $eventnewsRepository;

    /**
     * Inject a mynews repository to enable DI
     *
     * @param \Parousia\Mynews\Domain\Repository\MynewsRepository $mynewsRepository
     */
    public function injectPersoonRepository(\Parousia\Mynews\Domain\Repository\MynewsRepository $mynewsRepository)
    {
        $this->eventnewsRepository = $mynewsRepository;
    }
 
    /**
     * Evens List view
     *
     */
    public function eventsListAction(): ResponseInterface
    {

        $newsRecords = $this->eventnewsRepository->findEvents();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].' newsrecords'.html_entity_decode ( http_build_query($newsRecords,'',', '))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/mynews/Classes/Controller/debug.log');
		$mysunday= GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('mynews','pagesunday');
		if (substr($mysunday,0,1)!='/') $mysunday='/index.php?id='.$mysunday;
		$pageArguments = $this->request->getAttribute('routing');
		$pageId = $pageArguments->getPageId();

	    $assignedValues = [
            'news' => $newsRecords,
			'mysynday' => $mysunday,
            'currentPageId' => $pageId,
        ];
        //$assignedValues = $this->emitActionSignal('NewsController', self::SIGNAL_NEWS_EVENTSLIST_ACTION, $assignedValues);
        $this->view->assignMultiple($assignedValues); 
		return $this->htmlResponse();
    }

}
