<?php
defined('TYPO3') or die();

$GLOBALS['TCA']['tx_news_domain_model_news']['columns']['fal_media']['config']['appearance']['fileUploadAllowed']=0;
$fields = [
    'is_event' => [
        'exclude' => true,
        'label' => 'LLL:EXT:mynews/Resources/Private/Language/locallang_db.xlf:tx_eventnews_domain_model_news.is_event',
        'config' => [
            'type' => 'check',
            'default' => 0
        ],
		'onChange' => 'reload',
    ],
    'event_start' => [
        'exclude' => true,
        'displayCond' => 'FIELD:is_event:>:0',
        'label' => 'LLL:EXT:mynews/Resources/Private/Language/locallang_db.xlf:tx_eventnews_domain_model_news.event_start',
        'config' => [
            'type' => 'input',
            'size' => 12,
            'eval' => 'datetime',
			'renderType' => 'inputDateTime',
            'checkbox' => 0,
        ],
		'displayCond' => 'FIELD:is_event:>:0',
    ],
    'event_end' => [
        'exclude' => true,
        'displayCond' => 'FIELD:is_event:>:0',
        'label' => 'LLL:EXT:mynews/Resources/Private/Language/locallang_db.xlf:tx_eventnews_domain_model_news.event_end',
        'config' => [
            'default' => 0,
            'type' => 'input',
            'size' => 12,
            'eval' => 'datetime,int',
			'renderType' => 'inputDateTime',
        ],
		'displayCond' => 'FIELD:is_event:>:0',
    ],
];

$GLOBALS['TCA']['tx_news_domain_model_news']['palettes']['palette_event'] = [
    'canNotCollapse' => true,
    'showitem' => 'event_start,event_end'
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tx_news_domain_model_news', $fields);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tx_news_domain_model_news', 'is_event,--palette--;;palette_event', '', 'after:title');
