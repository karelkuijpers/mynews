<?php
if (!defined('TYPO3')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
         'Mynews',
         'Mynews',
         [
             \Parousia\Mynews\Controller\MynewsController::Class => 'eventsList',
         ],
         // non-cacheable actions
		 [
             \Parousia\Mynews\Controller\MynewsController::Class => 'eventsList',
         ],
     );

$GLOBALS['TYPO3_CONF_VARS']['EXT']['news']['classes']['Domain/Model/News']['mynews'] = 'mynews';
