<?php
ini_set("display_errors",1);
ini_set("log_errors",1);
$aPath=explode('/',dirname(__FILE__));
$newpath='/'.$aPath[1].'/'.$aPath[2].'/'.$aPath[3].'/vendor/mk-j/php_xlsxwriter/xlsxwriter.class.php';
include_once($newpath);
//include $_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchpersreg/Classes/Hooks/connectdb.php';
mb_internal_encoding("UTF-8");
$Typo3Version=GeneralUtility::makeInstance(Typo3Version::class)->getMajorVersion();
if ($Typo3Version<12)
	$localConf = include $_SERVER['DOCUMENT_ROOT'].'/typo3conf/LocalConfiguration.php';
else 
	$localConf = include str_replace("/public","",$_SERVER['DOCUMENT_ROOT']).'/config/system/settings.php';
$db = new mysqli($localConf['DB']['Connections']['Default']['host'], $localConf['DB']['Connections']['Default']['user'], $localConf['DB']['Connections']['Default']['password'], $localConf['DB']['Connections']['Default']['dbname']) or die("Unable to connect to SQL server");;
$db->set_charset("utf8");
$db->query("set session sql_mode = ''");

/*
* file to clean sermons older than 2 years
 * Created on 14-03-2019
*/
// Zoek oude newsberichten van gevoelige zendelingen:
$statement="SELECT uid,pid,from_unixtime(datetime) as datum,title,`teaser`,`bodytext`from `tx_news_domain_model_news` where endtime >0 and from_unixtime(endtime) < NOW() and ".
"uid in (select uid_foreign from sys_category_record_mm where uid_local=4 and tablenames='tx_news_domain_model_news') order by datetime desc";
$result=$db->query($statement) or die("Can't perform Query:".$statement);
$aUidNews=array();
if (mysqli_num_rows($result)>0)
{
	$archive=array();
	while ($row = $result->fetch_array(MYSQLI_ASSOC))
	{
		$text=$row['teaser']."\r\n".preg_replace('/\s\s+/', ' ',preg_replace('/\<\/*([a-zA-Z\d\s\"_\-\/.\=\:\;\?\&\%@]+)\s*\/*\>/i','',html_entity_decode(str_replace('&nbsp;',' ',$row['bodytext']))));
		$archive[]=['datum'=> $row['datum'], 'titel' => $row['title'],'inhoud'=>$text];
		$aUidNews[]=$row['uid'];
	}
	$exportnews=ExportToXlsx($archive,'missionnewsarchive');

}
$cUidsNews=implode(',',$aUidNews);
if (!empty($cUidsNews))
{
	// remove sys_file_reference
	$statement="delete FROM `sys_file_reference` WHERE `tablenames`='tx_news_domain_model_news' and deleted=0 and uid_foreign in (".$cUidsNews.")";
	//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": clean news sys_file_reference statement:".$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/cleannews.log');
	
	$result=$db->query($statement) or die("Can't perform Query:".$statement);
	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": clean news:".$db->affected_rows." news sys_file_reference removed"."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/cleannews.log');
	// delete news:
	$statement="delete from tx_news_domain_model_news where uid in (".$cUidsNews.")";
	//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": clean news tx_news_domain_model_news statement:".$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/cleannews.log');
	$result=$db->query($statement) or die("Can't perform Query:".$statement);
	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": clean news:".$db->affected_rows." news tx_news_domain_model_news removed"."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/cleannews.log');
}
// delete references to category:
$statement="delete from sys_category_record_mm where tablenames='tx_news_domain_model_news' and uid_foreign not in (select uid from tx_news_domain_model_news)";
//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": clean news sys_category_record_mm statement:".$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/cleannews.log');
$result=$db->query($statement) or die("Can't perform Query:".$statement);
error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": clean sys_category_record_mm:".$db->affected_rows." news tx_news_domain_model_news removed"."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/cleannews.log');

// delete files:
$statement="select identifier,`name`, uid,from_unixtime(`creation_date`) as datum, storage from sys_file where storage=(select uid from sys_file_storage where name='FALadmin') and uid not in (select uid_local as uid from `sys_file_reference`) ".
"and from_unixtime(creation_date) < DATE_SUB(NOW(),INTERVAL 3 YEAR) ".
"and identifier not like '/user_upload/Bestanden/Documenten/%' ".
"and identifier not like '/user_upload/Bestanden/Vacatures/%' ".
"and identifier not like '/user_upload/Bestanden/Iconen_en_logo_s/%' ".
"and identifier not like '/user_upload/Bestanden/01_Roosters/%' ".
"and identifier not like '/user_upload/Bestanden/02_Fotos_mensen/%' ".
"and identifier not like '/user_upload/Bestanden/Ledenvergadering/%' ".
"and identifier not like '/user_upload/Bestanden/03_Fotos_algemeen/%' ".
"and identifier not like '/user_upload/Bestanden/Fotos_vaste_paginas/%' ".
"and identifier not like '/user_upload/Bestanden/Zending/Vijge/Verhalen%' ".
"and identifier not like '/user_upload/Bestanden/Preken/%' ".
"order by identifier"; 
$result=$db->query($statement) or die("Can't perform Query:".$statement);
error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": delete files:".mysqli_num_rows($result)." files to be removed"."; statement:".$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/cleannews.log');
if (mysqli_num_rows($result)>0)
{
	$archivefiles=$result->fetch_all(MYSQLI_ASSOC);
	$exportfiles=ExportToXlsx($archivefiles,'filesarchive');
	$outdeleted=array();
	// Delete files:
	foreach ($archivefiles as $archivefile)
	{
		if (unlink('/var/FALadmin'.$archivefile['identifier'])) 
		{
			$msg=" ";
			$outdeleted[]=$archivefile['uid'];
		}
		else $msg=" could not be ";
		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": clean file:".$archivefile['identifier'].$msg."removed"."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/cleannews.log');
	}
	$uids=implode(",",$outdeleted);
	if (!empty($uids))
	{
		// delete sysfiles:
		$statement="delete from sys_file where uid in (".$uids.")";
		$result=$db->query($statement) or die("Can't delete sys_file Query:".$statement);
		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": clean sys_file:".$db->affected_rows." news sys_file removed"."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/cleannews.log');
		// delete filemetatdata:
		$statement="delete from sys_file_metadata where file in (".$uids.")";
		$result=$db->query($statement) or die("Can't delete sys_file_metadata Query:".$statement);
		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": clean filemetatdata:".$db->affected_rows." news metadata removed"."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/cleannews.log');
	}	

} 



function ExportToXlsx($aExport,$exportnaam,$sheetname= NULL,$header = NULL){
	$usermap=$_SERVER['DOCUMENT_ROOT'].'/Archief/';;
	// check if map exists:
	if (!file_exists($usermap))mkdir($usermap, 0777,true);

	$now=new \DateTime();
	$exportfilenaam=$usermap.'/'.$exportnaam.'_'.$now->format('Y-m-d H:i:s').".xlsx";;
	$eerstekeer=true;
	if (empty($sheetname)) $sheetname=$exportnaam;
	$writer = new XLSXWriter();     //new writer
	$data=array();
	if (empty($header))
	{
		if (is_array($aExport[0]))$header=array_keys($aExport[0]); 
		else $header=array();
	}
	//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ExportToXlsx header: '.http_build_query($header,'',', ')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/Classes/Controller/debug.log');
	$header=array_fill_keys($header,'string');
	//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ExportToXlsx aExport: '.http_build_query($aExport,'',', ')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/Classes/Controller/debug.log');
	if (is_array($aExport))
	{
		foreach ($aExport as $Export) array_push($data,array_values($Export));
	}
	$writer->writeSheet($data,$sheetname,$header);
	$writer->writeToFile($exportfilenaam);
	return $exportfilenaam;
}
