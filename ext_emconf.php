<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'MyNews',
    'description' => 'Templates for news extension, based on extbase & fluid.',
    'category' => 'fe',
    'author' => 'Karel Kuijpers',
    'author_email' => 'karelkuijpers@gmail.com',
    'state' => 'stable',
    'version' => '11.0.3',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.13-11.5.99',
            'news' => '8.4.0-',
        ],
        'conflicts' => [],
    ],
];
